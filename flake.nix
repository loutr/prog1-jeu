{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "nixpkgs/nixos-unstable";
  };

  outputs = { self, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        oPkgs = pkgs.ocamlPackages;
      in
      rec {
        defaultPackage = oPkgs.buildDunePackage {
          pname = "super_sudoku_3000";
          version = "0.1.0";

          duneVersion = "3";

          buildInputs = with oPkgs; [ graphics camlimages ];

          src = ./.;

          meta = with pkgs.lib; {
            description = "An staggeringly beautiful game of sudoku";
            homepage = "https://codeberg.org/loutr/prog1-jeu";
            license = licenses.unlicense;
          };
        };

        devShell = pkgs.mkShell {
          name = defaultPackage.pname + "-dev";
          packages = defaultPackage.buildInputs ++
            (with oPkgs; [ ocaml-lsp ocamlformat utop ]);
        };
      });
}
